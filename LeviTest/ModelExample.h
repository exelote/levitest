//
//  ModelExample.h
//  LeviTest
//
//  Created by Jonathan Banga on 24/4/15.
//  Copyright (c) 2015 LeviTest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelExample : NSObject
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)NSNumber *follow;

+ (instancetype)modelWithTitle:(NSString *)title follow:(NSNumber *)follow;

@end
