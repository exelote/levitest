//
//  AppDelegate.h
//  LeviTest
//
//  Created by Jonathan Banga on 24/4/15.
//  Copyright (c) 2015 LeviTest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

