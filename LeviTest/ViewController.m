//
//  ViewController.m
//  LeviTest
//
//  Created by Jonathan Banga on 24/4/15.
//  Copyright (c) 2015 LeviTest. All rights reserved.
//

#import "ViewController.h"
#import "ExampleTableViewCell.h"
#import "ModelExample.h"

#define KCellIdentifier @"CellIdentifier"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *data;
@end

@implementation ViewController

- (NSArray *)mockData{
    return @[
             [ModelExample modelWithTitle:@"Black Tie" follow:@(NO)],
             [ModelExample modelWithTitle:@"Active Wear" follow:@(NO)],
             [ModelExample modelWithTitle:@"Desk to Drinks" follow:@(NO)],
             [ModelExample modelWithTitle:@"Professional Chic" follow:@(NO)],
             [ModelExample modelWithTitle:@"Editors Picks" follow:@(NO)],
             ];
}


- (void)loadDataArray{
    self.data = [self mockData];
}

- (IBAction)saveDataArray{
    NSMutableArray *arrayToSendToTheWebService = [NSMutableArray new];
    for (ModelExample *model in self.data) {
        [arrayToSendToTheWebService addObject:@{@"Title":model.title,
                                                @"Follow":model.follow}];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadDataArray];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ExampleTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:KCellIdentifier];
}

#pragma mark - Table view data source and delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ExampleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:KCellIdentifier forIndexPath:indexPath];
    [cell fillWithModel:self.data[indexPath.row]];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ModelExample *selectedModel = self.data[indexPath.row];
    selectedModel.follow = [NSNumber numberWithBool:!selectedModel.follow.boolValue];
    ExampleTableViewCell *cell = (ExampleTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell fillWithModel:selectedModel];
    [self.tableView reloadData];
}



@end
