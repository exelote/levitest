//
//  ExampleTableViewCell.m
//  LeviTest
//
//  Created by Jonathan Banga on 24/4/15.
//  Copyright (c) 2015 LeviTest. All rights reserved.
//

#import "ExampleTableViewCell.h"
#import "ModelExample.h"

@interface ExampleTableViewCell()
@property(nonatomic,weak)IBOutlet UILabel *titleLabel;
@property(nonatomic,weak)IBOutlet UILabel *followLabel;
@end

@implementation ExampleTableViewCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:.3];
        self.contentView.backgroundColor = [UIColor colorWithWhite:0 alpha:.3];
    }
    return self;
}

- (void)fillWithModel:(ModelExample *)model{
    self.titleLabel.text = model.title;
    self.followLabel.text = model.follow.boolValue?@"Follow":@"Unfollow";
}

@end
