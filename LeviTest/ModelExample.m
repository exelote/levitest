//
//  ModelExample.m
//  LeviTest
//
//  Created by Jonathan Banga on 24/4/15.
//  Copyright (c) 2015 LeviTest. All rights reserved.
//

#import "ModelExample.h"

@implementation ModelExample

+ (instancetype)modelWithTitle:(NSString *)title follow:(NSNumber *)follow{
    ModelExample *example = [ModelExample new];
    if (example) {
        example.title = title;
        example.follow = follow;
    }
    return example;
}

@end
